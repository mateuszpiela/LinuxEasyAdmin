﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Renci.SshNet;

namespace LinuxEasyAdmin
{
    public partial class Opcje : Form
    {
        public SshClient ssh;
        private Logowanie login;
        public bool root;
        public string username;
        public string password_nonroot;
        public Opcje(string ip,int port,string username,string password,Logowanie login)
        {
            InitializeComponent();
            this.login = login;
            this.username = username;
            if(username == "root")
            {
                root = true;
            }
            else
            {
                root = false;
                password_nonroot = password;
            }
            ssh = new SshClient(ip, port, username, password);
            try
            {
                ssh.Connect();
            }
            catch
            {

            }
            if(ssh.ConnectionInfo.IsAuthenticated)
            {
                MessageBox.Show("Zostałeś zalogowany", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                login.Hide();
            }
            else
            {
                MessageBox.Show("Błąd: Nie można zalogować się do systemu sprawdż nazwę użytkownika lub hasło", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Uzytkownicy users = new Uzytkownicy(this);
            users.Show();
        }

        private void Opcje_FormClosing(object sender, FormClosingEventArgs e)
        {
            ssh.Disconnect();
            login.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Fail2ban.Fail2ban f2b = new Fail2ban.Fail2ban(this);
            f2b.Show();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ZHasla chpasswd = new ZHasla(this);
            chpasswd.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            OProgramie op = new OProgramie();
            op.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            RKHunter.RKHunter rk = new RKHunter.RKHunter(this);
            rk.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Wolne_miejsce.Wolne_miejsce wm = new Wolne_miejsce.Wolne_miejsce(this);
            wm.Show();
        }
    }

}
