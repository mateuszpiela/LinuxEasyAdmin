﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LinuxEasyAdmin
{
    public partial class Uzytkownicy_Haslo : Form
    {
        Uzytkownicy users;
        string username;
        public Uzytkownicy_Haslo(Uzytkownicy users, string username)
        {
            this.users = users;
            this.username = username;

            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == string.Empty)
            {
                MessageBox.Show("Proszę podać nowe hasło", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                string passwd = textBox1.Text.ToString();
                users.Zmienhaslo_uzytkownika(username, passwd);
                MessageBox.Show("Hasło zostało zmienione", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Dispose();
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
