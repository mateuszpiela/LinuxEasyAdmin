﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Renci.SshNet;
using CryptSharp;

namespace LinuxEasyAdmin
{
    public partial class Uzytkownicy : Form
    {
        public Opcje op;
        public Uzytkownicy(Opcje op)
        {
            this.op = op;
            InitializeComponent();
            
            
        }

        private void Uzytkownicy_Load(object sender, EventArgs e)
        {
            Odswiez_Uzytkownikow();
        }

        public void Odswiez_Uzytkownikow()
        {
            listBox1.Items.Clear();
            string command = "";
            if (op.root)
            {
                command = "cut -d: -f1 /etc/passwd";
            }
            else
            {
                command = "echo" + " '" + op.password_nonroot + "' " + "| sudo -S cut -d: -f1 /etc/passwd";
            }
            SshCommand cmd = op.ssh.CreateCommand(command);
            cmd.Execute();
            string res = cmd.Result;
            res = res.Replace("\n", ",");
            List<string> users = res.Split(',').ToList();
            users.Remove("");

            int counter = 0;
            while (users.Count != counter)
            {
                listBox1.Items.Add(users.ElementAt(counter));
                counter++;
            }
        }

        public void Zmienhaslo_uzytkownika(string username, string password)
        {
            string command = "";
            string salt = Crypter.Sha512.GenerateSalt();
            string hash = Crypter.Sha512.Crypt(password, salt);
            if (op.root)
            {
                command = "usermod -p " + hash + " " + username;
            }
            else
            {
                command = "echo '" + op.password_nonroot +"' sudo -S usermod -p " + hash + " " + username;
            }
            
            SshCommand cmd = op.ssh.CreateCommand(command);
            
            cmd.Execute();
        }

        public void Dodaj_uzytkownika(string username, string password, bool admin)
        {
            string command;
            if (op.root)
            {
                if (admin)
                {
                    command = "useradd -m -U -p " + password + " " + username + " " + "&& usermod -a -G adm,sudo " + username;
                }
                else
                {
                    command = "useradd -m -U -p " + password + " " + username;
                }
                SshCommand cmd = op.ssh.CreateCommand(command);
                cmd.Execute();
            }
            else
            {
                if (admin)
                {
                    command = "echo '" + op.password_nonroot + "' | sudo -S useradd -m -U -p " + password + " " + username + " " + "&& usermod -a -G adm,sudo " + username;
                }
                else
                {
                    command = "echo '" + op.password_nonroot + "' | sudo -S useradd -m -U -p " + password + " " + username;
                }
                SshCommand cmd = op.ssh.CreateCommand(command);
                cmd.Execute();
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Uzytkownicy_Dodaj useradd = new Uzytkownicy_Dodaj(this);
            useradd.Show();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
           string username = listBox1.SelectedItem.ToString();
            Uzytkownicy_Haslo chpasswd = new Uzytkownicy_Haslo(this, username);
            chpasswd.Show();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Czy chcesz na pewno usunąć użytkownika: " + listBox1.SelectedItem.ToString(), "Potwierdź usunięcie użytkownika", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                if (op.root)
                {
                    SshCommand cmd = op.ssh.CreateCommand("userdel -r " + listBox1.SelectedItem.ToString());
                    cmd.Execute();
                    Odswiez_Uzytkownikow();
                }
                else
                {
                    SshCommand cmd = op.ssh.CreateCommand("echo '" + op.password_nonroot + "' | sudo -S  userdel -r " + listBox1.SelectedItem.ToString());
                    cmd.Execute();
                    Odswiez_Uzytkownikow();
                }
            }
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            Odswiez_Uzytkownikow();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            button2.Enabled = true;
            button3.Enabled = true;
        }

        private void button6_Click(object sender, EventArgs e)
        {
           string username = listBox1.SelectedItem.ToString();
            Uzytkownicy_Typ uzytkowniktyp = new Uzytkownicy_Typ(username,this);
            uzytkowniktyp.Show();
        }
    }
}
