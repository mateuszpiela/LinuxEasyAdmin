﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Renci.SshNet;

namespace LinuxEasyAdmin
{ 
    public partial class Uzytkownicy_Typ : Form
    {
        Uzytkownicy users;
        string username;
        public Uzytkownicy_Typ(string username,Uzytkownicy users)
        {
            this.users = users;
            this.username = username;
            InitializeComponent();
            SprawdzGrupe();
        }

        private void Uzytkownicy_Typ_Load(object sender, EventArgs e)
        {
            
        }

        public void SprawdzGrupe()
        {
            SshCommand cmd = users.op.ssh.CreateCommand("groups " + username);
            cmd.Execute();
            string result = cmd.Result;
            if (result.Contains("adm"))
            {
                radioButton1.Checked = true;
                radioButton2.Checked = false;
            }
            else
            {
                radioButton1.Checked = false;
                radioButton2.Checked = true;
            }
                
        }

        private void radioButton2_MouseClick(object sender, MouseEventArgs e)
        {
            radioButton1.Checked = false;
        }

        private void radioButton1_MouseClick(object sender, MouseEventArgs e)
        {
            radioButton2.Checked = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string command = "";
            if (users.op.root)
            {
                
                if (radioButton1.Checked == true && radioButton2.Checked == false)
                {
                    command = "usermod -a -G adm,sudo " + username;
                }
                else if (radioButton2.Checked == true && radioButton1.Checked == false)
                {
                    command = "gpasswd -d " + username + " adm && gpasswd -d " + username + " sudo";
                }
            }
            else
            {
                if (radioButton1.Checked == true && radioButton2.Checked == false)
                {
                    command = "echo '" + users.op.password_nonroot + "' | sudo -S usermod -a -G adm,sudo " + username;
                }
                else if (radioButton2.Checked == true && radioButton1.Checked == false)
                {
                    command = "echo '" + users.op.password_nonroot + "' | sudo -S gpasswd -d " + username + " adm && gpasswd -d " + username + " sudo";
                }
            }
            SshCommand cmd = users.op.ssh.CreateCommand(command);
            cmd.Execute();
            MessageBox.Show("Wykonano", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Question);
            Dispose();
        }
    }
}
