﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LinuxEasyAdmin
{
    public partial class Uzytkownicy_Dodaj : Form
    {
        Uzytkownicy users;
        public Uzytkownicy_Dodaj(Uzytkownicy users)
        {
            this.users = users;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == string.Empty)
            {
                MessageBox.Show("Nie podano nazwy użytkownika", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                if (textBox2.Text == string.Empty)
                {
                    MessageBox.Show("Nie podano hasła", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else

                {
                    if (radioButton1.Checked == false && radioButton2.Checked == false)
                    {
                        MessageBox.Show("Wymagane zaznaczenie typu konta", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        string username = textBox1.Text.ToString();
                        string password = textBox2.Text.ToString();
                        bool admin = false;

                        if (radioButton1.Checked == true && radioButton2.Checked == false)
                        {
                            admin = false;
                        }
                        else if (radioButton2.Checked == true && radioButton1.Checked == false)
                        {
                            admin = true;
                        }

                        users.Dodaj_uzytkownika(username, password, admin);
                        users.Odswiez_Uzytkownikow();

                        Dispose();
                    }
                }
            }
        }

        private void radioButton1_MouseClick(object sender, MouseEventArgs e)
        {
            radioButton2.Checked = false;
        }

        private void radioButton2_MouseClick(object sender, MouseEventArgs e)
        {
            radioButton1.Checked = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
