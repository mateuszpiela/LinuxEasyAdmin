﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace LinuxEasyAdmin
{

    public partial class Logowanie : Form
    {
        string appdir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LinuxAdmin");

        public Logowanie()
        {
            InitializeComponent();
            PrzypomnijIP();
        }
        public void ZapamietajIP()
        {
            if (!comboBox1.Items.Contains(comboBox1.Text.ToString()))
            {
                string ipfile = appdir + "\\logip.txt";
                string ip = comboBox1.Text.ToString();
                string nl = ip + Environment.NewLine;


                File.AppendAllText(ipfile, nl);

            }
            
        }

        public void PrzypomnijIP()
        {
            int counter = 0;
            string line;
            string ipfile = appdir + "\\logip.txt";
            if (!Directory.Exists(appdir))
            {
                Directory.CreateDirectory(appdir);
            }
            if (File.Exists(ipfile))
            {
                System.IO.StreamReader file = new System.IO.StreamReader(ipfile);
                while((line = file.ReadLine()) != null)
                {
                    comboBox1.Items.Add(line);
                    counter++;
                }
                file.Close();

            }
            else
            {

            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            string ip = comboBox1.Text.ToString();
            string username = textBox1.Text.ToString();
            string password = textBox2.Text.ToString();
            int port = 22;
            if(ip.Contains(":"))
            {
                string res = ip.Substring(ip.LastIndexOf(":") + 1);
                string rep = ip.Substring(ip.LastIndexOf(":"));
                Int32.TryParse(res, out port);
                ip = ip.Replace(rep, "");

            }
            ZapamietajIP();



            Opcje op = new Opcje(ip,port,username,password,this);
            try
            {
                op.Show();
            }
            catch
            {

            }

        }

        private void Logowanie_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            OProgramie op = new OProgramie();
            op.Show();
        }
    }
}
