﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Renci.SshNet;

namespace LinuxEasyAdmin.Wolne_miejsce
{
    public partial class Wolne_miejsce : Form
    {
        Opcje op;
        public Wolne_miejsce(Opcje op)
        {
            this.op = op;
            InitializeComponent();
            Refresh_data();
        }

        private void Refresh_data()
        {
            richTextBox1.Text = "";
            SshCommand cmd = op.ssh.CreateCommand("df -h --type btrfs --type ext4 --type ext3 --type ext2 --type vfat --type iso9660 --type ntfs");
            cmd.Execute();
            richTextBox1.Text = cmd.Result;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Refresh_data();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
