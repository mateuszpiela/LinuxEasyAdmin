﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Renci.SshNet;
 
namespace LinuxEasyAdmin.Fail2ban
{
    
    public partial class Fail2ban : Form
    {
        public Opcje op;
        public Fail2ban(Opcje op)
        {
            this.op = op;
            InitializeComponent();
            Odswiez_klatki();
        } 

        public void Odswiez_klatki()
        {
            string command = "";
            if (op.root)
            {
                command = "fail2ban-client status |grep 'Jail list' | sed -e 's/`- Jail list://' | sed -e 's/[[:blank:]]//'";
            }
            else
            {
                command = "echo '" + op.password_nonroot + "' | sudo -S fail2ban-client status |grep 'Jail list' | sed -e 's/`- Jail list://' | sed -e 's/[[:blank:]]//'";
            }
            SshCommand cmd = op.ssh.CreateCommand(command);
            cmd.Execute(); 
            string x = cmd.Result;
            if(x.Contains(" "))
                x = x.Replace(" ", "");
            if (x.Contains("\n"))
                x = x.Replace("\n", "");
            
            List<string> jails = x.Split(',').ToList();

            jails.Remove("");
            int counter = 0;
            while (jails.Count != counter)
            {
                listBox1.Items.Add(jails.ElementAt(counter));
                counter++;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            string command = "";
            if (op.root)
            {
                command = "fail2ban-client start " + listBox1.SelectedItem.ToString();
            }
            else
            {
                command = "echo '" + op.password_nonroot + "' sudo -S fail2ban-client start " + listBox1.SelectedItem.ToString();
            }
            SshCommand cmd = op.ssh.CreateCommand(command);
            cmd.Execute();
            if (cmd.Result.Contains("started"))
                MessageBox.Show("Włączono", "Włączono", MessageBoxButtons.OK, MessageBoxIcon.Stop);

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string command = "";
            if (op.root)
            {
                command = "fail2ban-client stop " + listBox1.SelectedItem.ToString();
            }
            else
            {
                command = "echo '" + op.password_nonroot + "' sudo -S fail2ban-client stop " + listBox1.SelectedItem.ToString();
            }
            SshCommand cmd = op.ssh.CreateCommand(command);
            cmd.Execute();
            if (cmd.Result.Contains("stopped"))
                MessageBox.Show("Wyłączono", "Wyłączono", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string command = "";
            if (op.root)
            {
                command = "fail2ban-client reload";
            }
            else
            {
                command = "echo '" + op.password_nonroot + "' sudo -S fail2ban-client reload";
            }
            SshCommand cmd = op.ssh.CreateCommand(command);
            cmd.Execute();
            MessageBox.Show("Przeładowano", "Przeładowano", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Odswiez_klatki();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string jail = listBox1.SelectedItem.ToString();
            Fail2ban_ip f2b = new Fail2ban_ip(jail,this);
            f2b.Show();
        }
    }
}
