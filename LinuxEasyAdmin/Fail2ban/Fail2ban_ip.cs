﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Renci.SshNet;

namespace LinuxEasyAdmin.Fail2ban
{
    public partial class Fail2ban_ip : Form
    {
        public Fail2ban f2b;
        string jail;
        public Fail2ban_ip(string jail,Fail2ban f2b)
        {
            this.f2b = f2b;
            this.jail = jail;
            InitializeComponent();
        }

        public void Odswiez_ip(string jail)
        {
            listBox1.Items.Clear();
            string command = "";
            if (f2b.op.root)
            {
                command = "fail2ban-client status " + jail + " | grep 'Banned IP list' | sed -e 's/`- Banned IP list://' | sed -e 's/^[ \t]*//'";
            }
            else
            {
                command = "echo '" + f2b.op.password_nonroot + "' | sudo -S fail2ban-client status " + jail + " |grep 'Banned IP list' | sed -e 's/`- Banned IP list:' | sed -e 's/^[ \t]*//'";
            }
            SshCommand cmd = f2b.op.ssh.CreateCommand(command);
            cmd.Execute();
            string x = cmd.Result;
            if (x.Contains(" "))
                x = x.Replace(" ", "");
            if (x.Contains("\n"))
                x = x.Replace("\n", "");

            List<string> ips = x.Split(',').ToList();

            ips.Remove("");
            int counter = 0;
            while (ips.Count != counter)
            {
                listBox1.Items.Add(ips.ElementAt(counter));
                counter++;
            }
        }


        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            button1.Enabled = true;
        }

        private void Fail2ban_ip_odbanuj_Load(object sender, EventArgs e)
        {
            Odswiez_ip(jail);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string command;
            if (f2b.op.root)
            {
                command = "fail2ban-client set " + jail + " unbanip " + listBox1.SelectedItem.ToString();
            }
            else
            {
                command = "echo '" + f2b.op.password_nonroot + "' | sudo -S fail2ban-client set " + jail + " unbanip " + listBox1.SelectedItem.ToString();
            }
            SshCommand cmd = f2b.op.ssh.CreateCommand(command);
            cmd.Execute();

            MessageBox.Show("Odbanowano", "Odbanowano", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            Odswiez_ip(jail);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Fail2ban_ip_ban f2bipban = new Fail2ban_ip_ban(this, jail);
            f2bipban.Show();
        }
    }
}
