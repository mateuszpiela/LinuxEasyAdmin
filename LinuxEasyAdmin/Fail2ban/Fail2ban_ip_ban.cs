﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Renci.SshNet;

namespace LinuxEasyAdmin.Fail2ban
{
    public partial class Fail2ban_ip_ban : Form
    {
        Fail2ban_ip f2b;
        string jail;
        public Fail2ban_ip_ban(Fail2ban_ip f2b,string jail)
        {
            this.f2b = f2b;
            this.jail = jail;
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox1.Text == string.Empty)
            {
                MessageBox.Show("Proszę wypełnić adres ip przed zbanowaniem", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            else
            {
                string command;
                if (f2b.f2b.op.root)
                {
                    command = "fail2ban-client set " + jail + " banip " + textBox1.Text.ToString();
                }
                else
                {
                    command = "echo '" + f2b.f2b.op.password_nonroot + "' | sudo -S fail2ban-client set " + jail + " banip " + textBox1.Text.ToString();
                }
                SshCommand cmd = f2b.f2b.op.ssh.CreateCommand(command);
                cmd.Execute();

                MessageBox.Show("Zbanowano", "Zbanowano", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                f2b.Odswiez_ip(jail);
                Dispose();
            }
        }
    }
}
