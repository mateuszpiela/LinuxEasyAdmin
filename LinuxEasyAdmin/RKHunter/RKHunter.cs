﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Renci.SshNet;

namespace LinuxEasyAdmin.RKHunter
{
    public partial class RKHunter : Form
    {
        public Opcje op;
        public RKHunter(Opcje op)
        {
            this.op = op;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string command = "";
            if (op.root)
            {
                command = "rkhunter --sk -c --nocolors";
            }
            else
            {
                command = "echo '" + op.password_nonroot + "' | sudo -S rkhunter --sk -c --nocolors";
            }
            SshCommand cmd = op.ssh.CreateCommand(command);
            richTextBox1.Text = "Rozpoczęto skanowanie systemu. program może przez chwilę nie odpowiadać!";
            cmd.Execute();
            richTextBox1.Text = cmd.Result;
            button2.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            RKHunter_log rkhlog = new RKHunter_log(this);
            rkhlog.Show();
        }
    }
}
