﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Renci.SshNet;

namespace LinuxEasyAdmin.RKHunter
{
    public partial class RKHunter_log : Form
    {
        RKHunter rkh;
        public RKHunter_log(RKHunter rkh)
        {
            this.rkh = rkh;
            InitializeComponent();
            Refresh_log();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void Refresh_log()
        {
            richTextBox1.Text = "";
            string command = "";
            if (rkh.op.root)
            {
                command = "cat /var/log/rkhunter.log";
            }
            else
            {
                command = "echo '" + rkh.op.password_nonroot + "' | sudo -S cat /var/log/rkhunter.log";
            }

            SshCommand cmd = rkh.op.ssh.CreateCommand(command);
            cmd.Execute();
            richTextBox1.Text = cmd.Result;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Refresh_log();
        }
    }
}
