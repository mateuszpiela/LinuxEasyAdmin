﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Renci.SshNet;
using CryptSharp;

namespace LinuxEasyAdmin
{
    public partial class ZHasla : Form
    {
        Opcje op;
        public ZHasla(Opcje op)
        {
            this.op = op;
            InitializeComponent();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            
            string command = "";
            string username = op.username;
            string password = textBox1.Text.ToString();
            string salt = Crypter.Sha512.GenerateSalt();
            string hash = Crypter.Sha512.Crypt(password, salt);
            if (op.root)
            {
                command = "usermod -p " + hash + " " + username;
            }
            else
            {
                command = "echo '" + op.password_nonroot + "' sudo -S usermod -p " + hash + " " + username;
            }

            SshCommand cmd = op.ssh.CreateCommand(command);
            cmd.Execute();
            MessageBox.Show("Wykonano | Efekt można dopiero zobaczyć po ponownym połączeniu się z serwerem !", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            Dispose();
        }
    }
}
